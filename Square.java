package com.company;

public class Square extends Rectangle {
    Square() {
        super();
    }

    Square(double width) {
        super(width, width);
    }

    Square(double width, String color, boolean filled) {
        super(width, width, color, filled);
    }

    public double getSide() {
        return super.getWidth();
    }

    public void setSide(double width) {
        super.setLength(width);
        super.setWidth(width);
    }

    @Override
    public String toString() {
        return "Square[" + super.toString() + "]";
    }

    @Override
    public void setWidth(double width) {
        setSide(width);
    }

    @Override
    public void setLength(double length) {
        setSide(length);
    }
}