package com.company;

public class Circle extends Shape {
    private double radius;

    Circle() {
        super();
        radius = 1.0;
    }

    Circle(double radius) {
        super();
        this.radius = radius;
    }

    Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return (Math.PI) * (Math.pow(this.radius, 2));
    }

    public double getPerimetr() {
        return (Math.PI) * (2) * (this.radius);
    }

    @Override
    public String toString() {
        return "Circle[ " +
                "radius= " + radius +
                " " +
                "[" + super.toString() + "]";
    }
}
