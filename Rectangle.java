package com.company;

public class Rectangle extends Shape {
    private double width;
    private double length;

    Rectangle() {
        super();
        this.length = 1.0;
        this.width = 1.0;
    }

    Rectangle(double length, double width) {
        super();
        this.length = length;
        this.width = width;
    }

    Rectangle(double length, double width, String color, boolean filled) {
        super(color, filled);
        this.length = length;
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea() {
        return this.length * this.width;
    }

    public double getPerimetr() {
        return (this.length + this.width) * 2;
    }

    @Override
    public String toString() {
        return "Rectangle[ " +
                "width=" + width +
                ", length=" + length +
                " [" + super.toString() + "]";
    }
}
