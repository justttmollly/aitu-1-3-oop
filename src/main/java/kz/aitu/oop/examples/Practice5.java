package kz.aitu.oop.examples;

import java.io.File;

public class Practice5 {
    public static void main (String[] args) {
        String str = new String();
        try {
            throw new Exception("throwing an exception");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        File f = null;
        boolean bool = false;

        try {
            f = new File("test.txt");
            f.createNewFile();
            bool = f.exists();
            System.out.println("File exists: " + bool);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        public boolean isInt (String str){
            try {
                Integer.parseInt(str);
            } catch (NumberFormatException e) {
                return false;
            } catch (NullPointerException e) {
                return false;
            }
            return true;
        }
        /////zaebal gitlab

        public boolean isDouble(String str) {
            try {
                Double.parseDouble(str);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }
}