package kz.aitu.oop.solid.myitcompany;

public interface Admin extends Person {
    void administrate();
}