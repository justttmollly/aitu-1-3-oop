package kz.aitu.oop.solid.myitcompany;

public interface AndroidDeveloper extends Developer {
    void developAndroidApp();
}
