package kz.aitu.oop.solid.myitcompany;

public interface Employee extends Person {
    void work();
}