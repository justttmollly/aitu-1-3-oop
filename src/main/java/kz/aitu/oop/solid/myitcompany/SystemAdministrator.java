package kz.aitu.oop.solid.myitcompany;


public class SystemAdministrator extends Worker implements SysAdmin {
    public SystemAdministrator(String name) {
        super(name);
    }

    @Override
    public void administrate() {
        adminSys();
    }

    @Override
    public void work() {
        administrate();
    }

    @Override
    public void adminSys() {
        System.out.println("Hi there! My name is " + getName() + ", and I am a System Administrator");
    }
}
