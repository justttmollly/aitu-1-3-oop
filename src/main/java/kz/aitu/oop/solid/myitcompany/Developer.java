package kz.aitu.oop.solid.myitcompany;

public interface Developer extends Person {
    void develop();
}

