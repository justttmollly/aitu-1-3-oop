package kz.aitu.oop.solid.myitcompany;

public class Main {
    public static void main(String[] args) {
        Company company = new Company();
        company.addEmployee(new JavaDeveloper("Sa"));
        company.addEmployee(new SystemAdministrator("Eva"));

        company.startWork();
    }
}
