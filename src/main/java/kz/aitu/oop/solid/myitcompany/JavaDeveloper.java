package kz.aitu.oop.solid.myitcompany;


public class JavaDeveloper extends Worker implements AndroidDeveloper {
    public JavaDeveloper(String name) {
        super(name);
    }

    @Override
    public void develop() {
        developAndroidApp();
    }

    @Override
    public void work() {
        develop();
    }

    @Override
    public void developAndroidApp() {
        System.out.println("Hello, my name is " + getName() + ", and I am using Java to develop applications for Android");
    }
}
